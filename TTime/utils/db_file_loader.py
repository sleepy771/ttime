__author__ = 'filip'
class DbConfLoader(object):

    def __init__(self, option_selected, options_needing_configuration=tuple()):
        self._needs_conf = option_selected in options_needing_configuration
        self._can_fallback = option_selected in ('testing', 'development')
        if not self._needs_conf:
            return
        import os.path
        self._file_exists = os.path.exists(os.path.abspath('db_conf.py'))
        self._imported_conf = __import__('db_conf')

    @property
    def db_name(self):
        try:
            if not self._needs_conf:
                return None
            self._file_not_exist()
            if not hasattr(self._imported_conf, 'DATABASE_NAME'):
                raise Exception('Missing DATABASE_NAME in configuration file')
            return self._imported_conf.DATABASE_NAME
        except Exception:
            if self._can_fallback:
                return os.path.join(os.path.dirname(os.path.dirname(__file__)), 'db.sqlite3')
            raise

    @property
    def db_user(self):
        if not self._needs_conf:
            return None
        self._file_not_exist()
        if not hasattr(self._imported_conf, 'DATABASE_USER'):
            raise Exception('Missing DATABASE_USER in configuration file')
        return self._imported_conf.DATABASE_USER

    @property
    def db_password(self):
        if not self._needs_conf:
            return None
        self._file_not_exist()
        if not hasattr(self._imported_conf, 'DATABASE_NAME'):
            raise Exception('Missing DATABASE_PASSWORD in configuration file')
        return self._imported_conf.DATABASE_PASSWORD

    @property
    def db_host(self):
        if not self._needs_conf:
            return None
        self._file_not_exist()
        if not hasattr(self._imported_conf, 'DATABASE_HOST'):
            raise Exception('Missing DATABASE_HOST in configuration file')
        return self._imported_conf.DATABASE_HOST

    @property
    def db_engine(self):
        try:
            if not self._needs_conf:
                raise Exception()
            self._file_not_exist()
            if not hasattr(self._imported_conf, 'DATABASE_HOST'):
                raise Exception('Missing DATABASE_HOST in configuration file')
            return self._imported_conf.DATABASE_HOST
        except Exception:
            if self._can_falback:
                return 'django.db.backends.sqlite3'

    def _file_not_exist(self):
        if not self._file_exists:
            raise Exception('Configuration file not present')