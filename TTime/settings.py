"""
Django settings for TTime project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import db_conf
#from utils.db_file_loader import DbConfLoader

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEVELOPMENT = 'development'
PRODUCTION = 'production'
TESTING = 'testing'

SELECTED_OPTION = DEVELOPMENT

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ch@mi_bj)og0dhe36#361lrdph6i*oitjj_+9r#^u*46qoq0d_'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition
# userena, guardian and easy_thumbnails

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'userena',
    'guardian',
    'easy_thumbnails',
    'accounts',
    'timemanager',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'middleware.django-crossdomainxhr-middleware.XsSharing'
)

ROOT_URLCONF = 'TTime.urls'

WSGI_APPLICATION = 'TTime.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# db_conf_loader = DbConfLoader('db_conf.py', SELECTED_OPTION, (PRODUCTION, DEVELOPMENT))

DATABASES = {
    'default': {
        'ENGINE': db_conf.DATABASE_ENGINE,
        'NAME': db_conf.DATABASE_NAME,
        'USER': db_conf.DATABASE_USER,
        'PASSWORD': db_conf.DATABASE_PASSWORD,
        'HOST': db_conf.DATABASE_HOST,
        'PORT': db_conf.DATABASE_PORT
    },
    # PRODUCTION: {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': db_conf_loader.db_name,
    #     'USER': db_conf_loader.db_user,
    #     'PASSWORD': db_conf_loader.db_password,
    #     'HOST': db_conf_loader.db_host,
    #     'PORT': db_conf_loader.db_port,
    # },
    # DEVELOPMENT: {
    #
    # },
    # TESTING: {
    #
    # }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

ANONYMOUS_USER_ID = -1