from django.db import models


class GoalManager(models.Manager):

    def hasSimultaneousGoals(self, goal):
        if goal.parent is None:
            return False
        return self.filter(parent=goal.parent, order_number=goal.order_number).exists()


GOAL_CHOICES = (
    ('p', 'Postponable'),
    ('d', 'Due Date'),
    ('i', 'Important'),
)

class Goal(models.Model):
    # TODO bind it to User account
    # TODO goal might be bind with Event, as OneToOne relation
    equivalence_class = models.ForeignKey('goals.GoalEquivalenceClass')
    complete = models.BooleanField()
    parent = models.ForeignKey('goals.Goal')
    title = models.CharField(max_length=255)
    tags = models.ManyToManyRel('timemanager.Tag')
    category = models.ForeignKey('timemanager.Category')
    type = models.CharField(max_length=1);
    priority = models.IntegerField()
    order_number = models.IntegerField()

    objects = GoalManager()

    @property
    def hasSimultaneousGoals(self):
        # Check whether there are goals with equal order_number
        if self.parent is None:
            return False
        return Goal.objects.filter(parent=self.parent.pk, order_numer=self.order_number).exists()

    @property
    def can_postpone(self):
        return self.priority < 50


class DueDateGoal(Goal):
    due_date = models.DateTimeField(null=False, blank=False)


class PostponedGoal(DueDateGoal):

    class Meta:
        proxy = True

    def postpone_to(self, date):
        self.due_date = date


class GoalEquivalenceClassManager(models.Manager):
    pass


class GoalEquivalenceClass(models.Model):
    category = models.ForeignKey('timemanager.Category', related_name='goal_equivalence_classes')
    common_tags = models.ManyToManyRel('timemanager.Tag', related_name='goal_equivalence_classes')
