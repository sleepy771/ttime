from django.db import models
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

# Create your models here.


class Tag(models.Model):
    name = models.CharField(max_length=255, unique=True)


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)


class EventResource(ModelResource):


    def dehydrate_tags(self, bundle):
        return list((tag.name for tag in bundle.data['tags']))

    def dehydrate_category(self, bundle):
        return bundle.data['category'].name


class Event(models.Model):
    date_from = models.DateTimeField(null=False, blank=False)
    date_to = models.DateTimeField(null=True, blank=True)
    # tags = models.ManyToManyField(Tag)
    # category = models.ForeignKey(Category)

    @property
    def tags(self):
        if hasattr(self, 'tags'):
            return self.tags
        elif hasattr(self, 'goal'):
            return self.goal.tags
        else:
            raise Exception('This model can not be used standalone')

    @property
    def category(self):
        if hasattr(self, 'category'):
            return self.category
        elif hasattr(self, 'goal'):
            return self.goal.category
        else:
            raise Exception('This model can not be used standalone')

    @property
    def in_progress(self):
        return self.date_to is None


class NonGoalEvent(Event):
    tags = models.ManyToManyField(Tag)
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=255)