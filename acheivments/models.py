from django.db import models


class AchievementManager(models.Manager):
    pass


class Achievement(models.Model):
    goal = models.OneToOneField('goals.Goal')
    complete_sub_goals = models.IntegerField()
    all_goals = models.IntegerField()
    sub_goals_complete = models.ManyToManyRel('goals.Goal')
    took_time_in_millis = models.IntegerField()
    goal_subjective_score = models.IntegerField()

    objects = AchievementManager()