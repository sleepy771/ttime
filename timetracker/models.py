from django.db import models
from constants import CATEGORIES


class TimeTrackerTags(models.Model):
    tag = models.CharField(max_length=255)


class Event(models.Model):
    init_time = models.DateTimeField()
    fin_time = models.DateTimeField()
    title = models.CharField()
    description = models.TextField()
    tags = models.ManyToManyField(TimeTrackerTags, null=False, blank=False)
    category = models.CharField(max_length=1, blank=False, choices=CATEGORIES)